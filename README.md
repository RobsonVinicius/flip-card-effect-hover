# Flip Card Effect Hover

Flip Card com efeito hover desenvolvido para fins de estudo e treino

<br>


## Preview

<div align="center">
 
 <img src="preview.gif" min-width="700px" width="700px" align="center" alt="image">
  
</div>

## Demo

https://robsonvinicius.gitlab.io/flip-card-effect-hover/
